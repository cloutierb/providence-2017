## Install

Just need node and [yarn](https://yarnpkg.com/en/docs/install#mac-stable) installed then run

```
yarn install
npm start
```

Dev server works fine, no need to build it

## Notes

Point of this visualization is to show the largest property owners in Providence. Clicking on bubbles will drilldown. I am only showing number of properties owned per company greater than 15. This is easily configurable though. I didn't focus on making everything generic, I was more striving for an interesting visualization.

## Design decisions

- This is a small app, so using Redux or MobX is a bit overkill. I only had a small amount of state to manage and it was all only one level up. If I needed to go more levels down, then I could of used react context.
- Only one page, so I didn't use a router either.
- No thunk or sages, so all async calls are done directly in the component
- I used mostly CSS styles instead of inline. CSS styles give me more control and makes working with d3 a bit easier. All styles are namespace though.

## Ran out of time, but I wanted to add

- Clean up styles
- Fix the company titles. Right now everything is lowercase. I was getting 'City of Providence' and 'CITY OF PROVIDENCE' as two separate rows, so I just changed my query to make everything lower case.
- Having some animations when the bubbles are clicked would look cool.
- Slider that changes the max number of properties owned (currently fixed at 15).
- Showing tax information when clicking a property on the map
