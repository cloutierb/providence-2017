import reduce from 'lodash/reduce'
import forEach from 'lodash/forEach'
import sumBy from 'lodash/sumBy'

// Used one of the scales from d3-scale-chromatic
export const LEVY_CODES_COLOR_MAP = {
  'Owner Occ. Res.': '#1b9e77',
  'Non Owner Occ. Res.': '#d95f02',
  Commercial: '#7570b3',
  'Non Residential Exempt': '#e7298a',
  '8LAW': '#66a61e',
  TSA: '#e6ab02',
  PRA: '#a6761d',
  PRAC: '#666666'
}

// this shapes the data so that d3 pack can be used. Shape is defined in CommonPropTypes.
export const shapeLargestPropertyOwnersResults = results => {
  let newData = reduce(
    results,
    (newData, d) => {
      newData[d.levy_code_desc] = newData[d.levy_code_desc] || []
      newData[d.levy_code_desc].push({
        owner_company: d.owner_company,
        count: +d.count,
        fill: LEVY_CODES_COLOR_MAP[d.levy_code_desc]
      })
      return newData
    },
    {}
  )
  let r = []
  forEach(newData, (children, levy_code_desc) => {
    r.push({
      levy_code_desc,
      children,
      active: true,
      fill: LEVY_CODES_COLOR_MAP[levy_code_desc],
      count: sumBy(children, 'count')
    })
  })
  return r
}
