import PropTypes from 'prop-types'

export default {
  PropertyData: PropTypes.arrayOf(
    PropTypes.shape({
      class: PropTypes.string.isRequired,
      class_desc: PropTypes.string.isRequired,
      formatted_address: PropTypes.string,
      levy_code: PropTypes.string.isRequired,
      levy_code_desc: PropTypes.string.isRequired,
      location_city: PropTypes.string.isRequired,
      location_street: PropTypes.string,
      location_zip: PropTypes.string,
      lot: PropTypes.string.isRequired,
      owner_address: PropTypes.string,
      owner_city: PropTypes.string.isRequired,
      owner_location_address: PropTypes.string.isRequired,
      owner_location_city: PropTypes.string.isRequired,
      owner_location_state: PropTypes.string.isRequired,
      owner_location_zip: PropTypes.string,
      owner_state: PropTypes.string.isRequired,
      owner_street: PropTypes.string,
      plat: PropTypes.string.isRequired,
      plat_lot_unit: PropTypes.string.isRequired,
      property_location_address: PropTypes.string,
      property_location_city: PropTypes.string.isRequired,
      property_location_state: PropTypes.string.isRequired,
      property_location_zip: PropTypes.string,
      total_assessment: PropTypes.string.isRequired,
      total_exemption: PropTypes.string.isRequired,
      unit: PropTypes.string.isRequired
    })
  ),
  D3PackFormat: PropTypes.arrayOf(
    PropTypes.shape({
      active: PropTypes.bool.isRequired,
      count: PropTypes.number.isRequired,
      levy_code_desc: PropTypes.string.isRequired,
      fill: PropTypes.string.isRequired,
      children: PropTypes.arrayOf(
        PropTypes.shape({
          active: PropTypes.bool,
          count: PropTypes.number.isRequired,
          fill: PropTypes.string.isRequired,
          owner_company: PropTypes.string.isRequired
        })
      )
    })
  )
}
