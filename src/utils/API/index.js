import API from './API'
import fixtureAPI from './fixture'

export default (process.env.REACT_APP_MOCK_SERVER ? fixtureAPI : API.create())
