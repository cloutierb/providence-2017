import axios from 'axios'
import get from 'lodash/get'

const keys = {
  providenceData: 'JIxrwjdyV0h2g3zUZIMoUCFV5',
  mapBox: 'pk.eyJ1IjoiY2xvdXRpZXJiIiwiYSI6ImNqbDVjc2FraDJsbTIzd21wb2RrMnU2aWEifQ.rD1CFH-gGEF43gvFkBPxRw'
}

const create = (store = null) => {
  const api = axios.create({
    headers: {
      'X-App-Token': keys.providenceData
    },
    baseURL: 'https://data.providenceri.gov/resource/r6n7-qjr6.json',
    timeout: 20000
  })

  // Not using date range filters yet. Not sure if I have to.
  const getLargestPropertyOwners = (minNumProperties = '15', limit = 100) =>
    sendRequest({
      params: {
        $select: 'lower(owner_company) as owner_company, levy_code_desc, COUNT(owner_company) AS count',
        $group: 'owner_company, levy_code_desc',
        $where: 'owner_company IS NOT NULL',
        $order: 'count DESC',
        $having: `count > ${minNumProperties}`,
        $limit: limit
      }
    })

  const getPropertyOwned = owner_company =>
    sendRequest({
      params: {
        $select: 'property_location, total_exemption, total_taxes, total_assessment, owner_company',
        $where: `lower(owner_company) = '${owner_company}'`
      }
    })

  const sendRequest = requestObj => {
    const requestPromise = new Promise((resolve, reject) => {
      api
        .request(requestObj)
        .then(response => resolve(response.data))
        .catch(error => {
          const status = get(error, 'response.status')
          if (status !== 200) {
            reject(error)
          }
        })
    })

    return requestPromise
  }

  return {
    getLargestPropertyOwners,
    getPropertyOwned,
    keys
  }
}

export default {
  create
}
