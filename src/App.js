import React, { PureComponent } from 'react'

import Header from 'components/Header'
import LargestPropertyOwnersChart from 'components/LargestPropertyOwnersChart'
import Page from 'components/Page'

class App extends PureComponent {
  render() {
    return (
      <Page>
        <Header title="Largest property owners in Providence" />
        <LargestPropertyOwnersChart />
      </Page>
    )
  }
}

export default App
