import React, { Component } from 'react'
import { format } from 'd3-format'
import map from 'lodash/map'

import BubbleChart from 'components/BubbleChart'
import Map from 'components/Map'
import API from 'utils/API'
import { LEVY_CODES_COLOR_MAP, shapeLargestPropertyOwnersResults } from 'utils/helpers'

// Bubble chart formatters
const formatPropertyCount = format(',d')

const formatHoverTitle = d => `${d.levy_code_desc || d.owner_company}\n${formatPropertyCount(d.count)} properties`

const formatTextData = d =>
  d.data.levy_code_desc ? [d.data.levy_code_desc] : d.data.owner_company ? d.data.owner_company.split(' ') : []

class LargestPropertyOwnersChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      map: {} // holds the points that will be drawn on map tooltip
    }
  }

  componentDidMount() {
    API.getLargestPropertyOwners().then(data => {
      this.setState({ data: shapeLargestPropertyOwnersResults(data) })
    })
  }

  handleClick = node => {
    if (node.depth === 1) {
      // if the large bubble (levy code) is clicked, then set large bubble active to false (hides it) and sets children active
      const clickedOnBubble = node.data.levy_code_desc
      const data = map(this.state.data, d => {
        if (d.levy_code_desc === clickedOnBubble) {
          d.active = false
          d.children = map(d.children, c => ({ ...c, active: true }))
        }
        return d
      })

      this.setState({ data })
    } else if (node.depth === 2) {
      // if a company is clicked, then fetch location points for map and format geoJSON data
      return API.getPropertyOwned(node.data.owner_company).then(results => {
        const points = results.map(r => ({
          geometry: r.property_location,
          type: 'Feature'
        }))

        this.setState({
          map: {
            points
          }
        })
      })
    }
  }

  render() {
    return (
      <BubbleChart
        data={this.state.data}
        formatTextData={formatTextData}
        formatHoverTitle={formatHoverTitle}
        onClick={this.handleClick}
        maxDepth={2}
        legend={LEVY_CODES_COLOR_MAP}
        tooltip={<Map center={[-71.426, 41.8183]} zoom={11} points={this.state.map.points} />}
      />
    )
  }
}

export default LargestPropertyOwnersChart
