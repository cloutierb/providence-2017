import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import mapboxgl from 'mapbox-gl/dist/mapbox-gl'

import API from 'utils/API'
import './Map.css'

mapboxgl.accessToken = API.keys.mapBox

class Map extends PureComponent {
  componentDidMount() {
    this.map = new mapboxgl.Map({
      container: this.mapRef,
      style: 'mapbox://styles/mapbox/streets-v9',
      center: this.props.center,
      zoom: this.props.zoom
    })
  }

  componentWillUnmount() {
    this.map.remove()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.points !== this.props.points) {
      if (this.map.getSource('property-data')) {
        this.map.getSource('property-data').setData({
          type: 'FeatureCollection',
          features: this.props.points
        })
      } else {
        //referenced https://www.mapbox.com/mapbox-gl-js/example/cluster/
        this.map.addSource('property-data', {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: this.props.points
          },
          cluster: true,
          clusterMaxZoom: 14,
          clusterRadius: 50
        })

        this.map.addLayer({
          id: 'clusters',
          type: 'circle',
          source: 'property-data',
          filter: ['has', 'point_count'],
          paint: {
            'circle-color': ['step', ['get', 'point_count'], '#51bbd6', 100, '#f1f075', 750, '#f28cb1'],
            'circle-radius': ['step', ['get', 'point_count'], 20, 100, 30, 750, 40]
          }
        })

        this.map.addLayer({
          id: 'cluster-count',
          type: 'symbol',
          source: 'property-data',
          filter: ['has', 'point_count'],
          layout: {
            'text-field': '{point_count_abbreviated}',
            'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
            'text-size': 12
          }
        })

        this.map.addLayer({
          id: 'unclustered-point',
          type: 'circle',
          source: 'property-data',
          filter: ['!', ['has', 'point_count']],
          paint: {
            'circle-color': '#f28cb1',
            'circle-radius': 4,
            'circle-stroke-width': 1,
            'circle-stroke-color': '#fff'
          }
        })
      }
    }
  }

  render() {
    return <div className="map-container" ref={node => (this.mapRef = node)} />
  }
}

Map.propTypes = {
  center: PropTypes.arrayOf(PropTypes.number).isRequired,
  points: PropTypes.arrayOf(
    PropTypes.shape({
      geometry: PropTypes.shape({
        type: PropTypes.string.isRequired,
        coordinates: PropTypes.arrayOf(PropTypes.number).isRequired
      }),
      type: PropTypes.string.isRequired
    })
  ),
  zoom: PropTypes.number
}

Map.defaultPropTypes = {
  zoom: 0
}

export default Map
