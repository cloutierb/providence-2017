import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import './Header.css'

class Header extends PureComponent {
  render() {
    const { title, subtitle } = this.props

    return (
      <header className="header-container">
        <h1 className="header-title">{title}</h1>
        <h2 className="header-subtitle">{subtitle}</h2>
      </header>
    )
  }
}

Header.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string
}

export default Header
