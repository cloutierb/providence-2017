import React, { PureComponent } from 'react'
import './Page.css'

class App extends PureComponent {
  render() {
    const { children } = this.props

    return <div className="page-container">{children}</div>
  }
}

export default App
