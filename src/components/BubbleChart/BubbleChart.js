import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { select, event, selectAll } from 'd3-selection'
import { pack, hierarchy } from 'd3-hierarchy'
import map from 'lodash/map'

import CommonPropTypes from 'utils/CommonPropTypes'
import './BubbleChart.css'

const bubblePadding = 3

const packBubbleChart = (data, width, height) =>
  pack()
    .size([width, height])
    .padding(bubblePadding)(hierarchy({ children: data, data: { fill: 'rgba(0, 0, 0, 0)' } }).sum(d => d.count))

// I am just going to hardcode this, but you could pull it from the data

class BubbleChart extends Component {
  state = {}
  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.updateBubbleChart()
    }
  }

  updateBubbleChart = () => {
    const { height, width, data, formatHoverTitle, formatTextData } = this.props
    const root = packBubbleChart(data, height, width)

    // if props updates, then d3 needs to re-add everything
    select(this.bubbleChart)
      .selectAll('g')
      .remove()

    const svg = select(this.bubbleChart)

    // initialize svg
    svg
      .attr('height', height)
      .attr('width', width)
      .style('font', '10px sans-serif')
      .attr('text-anchor', 'middle')
      .on('click')

    //  create nodes
    const leaf = svg
      .selectAll('.node')
      .data(root.descendants())
      .enter()
      .append('g')
      .attr('class', d => (d.data.active ? 'node' : 'node disabled'))
      .attr('transform', d => `translate(${d.x + 1},${d.y + 1})`)

    // add circles
    leaf
      .append('circle')
      .attr('r', d => d.r)
      .attr('fill', d => d.data.fill)
      .on('click', this.handleClick)

    // add company names
    leaf
      .append('text')
      .selectAll('tspan')
      .data(d => formatTextData(d))
      .enter()
      .append('tspan')
      .attr('x', 0)
      .attr('y', (d, i, n) => 12 + (i - n.length / 2 - 0.5) * 7.5)
      .text(d => d)

    selectAll('text').on('click', this.handleClick)

    // add company name and property count when the user hovers
    if (formatHoverTitle) {
      leaf.append('title').text(d => formatHoverTitle(d.data))
    }
  }

  handleClick = d => {
    const e = event
    const tooltip = select(this.tooltip)
    if (d.depth === this.props.maxDepth) {
      this.props.onClick(d).then(() => {
        tooltip
          .style('display', 'block')
          .style('left', e.pageX - 150 + 'px')
          .style('top', e.pageY - 310 + 'px')
      })
    } else {
      this.props.onClick(d)
    }
  }

  handleCloseTooltip = () => select(this.tooltip).style('display', 'none')

  renderLegend() {
    return (
      <div className="bubble-chart-legend-container">
        {map(this.props.legend, (color, desc) => {
          return (
            <span key={`legend-${desc}`} className="bubble-chart-legend">
              <div className="bubble-chart-legend-key" style={{ backgroundColor: color }} />
              {`${desc}`}
            </span>
          )
        })}
      </div>
    )
  }

  render() {
    return (
      <section>
        {this.renderLegend()}
        <div className="bubble-chart-container" onClick={this.handleCloseTooltip}>
          <svg ref={node => (this.bubbleChart = node)} />
        </div>
        <div className="bubble-chart-tooltip" ref={node => (this.tooltip = node)}>
          {this.props.tooltip}
        </div>
      </section>
    )
  }
}

BubbleChart.propTypes = {
  data: CommonPropTypes.D3PackFormat,
  formatHoverTitle: PropTypes.func,
  formatTextData: PropTypes.func,
  height: PropTypes.number,
  legend: PropTypes.object,
  maxDepth: PropTypes.number, // could calculate maxDepth by looking at max depth of children in data
  onClick: PropTypes.func,
  tooltip: PropTypes.node,
  width: PropTypes.number
}

BubbleChart.defaultProps = {
  data: [],
  height: 932,
  onClick: () => {},
  formatTextData: t => t,
  width: 932
}

export default BubbleChart
